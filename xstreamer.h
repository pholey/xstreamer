#ifndef XSTREAMER_H
#define XSTREAMER_H

#include <QMainWindow>

namespace Ui {
class Xstreamer;
}

class Xstreamer : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit Xstreamer(QWidget *parent = 0);
    ~Xstreamer();
    
private slots:
    void on_pushButton_clicked();

    void on_fps_textChanged();

private:
    Ui::Xstreamer *ui;
};

#endif // XSTREAMER_H
