#include "xstreamer.h"
#include "ui_xstreamer.h"
#include <QString>
#include <QProcess>

Xstreamer::Xstreamer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Xstreamer)
{
    ui->setupUi(this);
}

Xstreamer::~Xstreamer()
{
    delete ui;
}

void Xstreamer::on_pushButton_clicked()
{
    QString streamid;
    streamid = ui->streamid->toPlainText();
    QString res;
    res = ui->inres->toPlainText();
    QString fps;
    fps = ui->fps->toPlainText();
    QString compilestream = "g++ S.cpp -o S";
    QString RunStream = "./S "+streamid+" "+res+" "+fps;
    QProcess::execute(compilestream);
    QProcess::execute(RunStream);
}

void Xstreamer::on_fps_textChanged()
{

}
