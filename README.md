Xstreamer
=========

a C++ wrapper for a Streaming Utility in Linux, using C++ and QT


Simple program made for ease of streaming to Twitch.tv.



Dependencies
============

You will need qmake installed, you could do this by simply typing "qmake --version" into your terminal, or:

$ sudo apt-get install libxtst-dev build-essential libqt4-dev qt4-qmake

Instructions for Compiling and Running:
=======================================

Create the MakeFile:

$ qmake Xstreamer.pro

OR

$ qmake-qt4 Xstreamer.pro

Compile:

$ make

Run:

$ ./Xstreamer


and just like that, you are done!

Running without a GUI:
======================

if you have problems with QT, you may opt out of the GUI by compiling "S.cpp"

compile S.cpp:

$ g++ S.cpp -o S

which will create the file "S"

then to run it, you need 3 arguments, in the order of streamID, your resolution, and desired FPS;

for example:

$ ./S stream_ID-1234567 1366x768 30

