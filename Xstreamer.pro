

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Xstreamer
TEMPLATE = app


SOURCES += main.cpp\
        xstreamer.cpp

HEADERS  += xstreamer.h

FORMS    += xstreamer.ui

INCLUDEPATH += #<your path>
